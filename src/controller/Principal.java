package controller;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.util.Duration;

/**
 * @author Daniel Silva Marcelino
 */
public class Principal extends Application {

    @FXML
    private Label labelSegundos;

    @FXML
    private Label labelMinutos;

    @FXML
    private Label labelHoras;

    @FXML
    private Label labelDias;

    @FXML
    private Label labelAnos;

    @FXML
    private Button buttonStart;

    @FXML
    private Button buttonStop;

    private final int DEZ = 10;
    private final int MINUTO = 60;
    private final int HORA = 24;
    private final int DIAS = 365;

    private int segundo = 1;
    private int minuto = 1;
    private int hora = 1;
    private int dia = 1;
    private int ano = 1;

    private static Stage stage;
    private Scene scene;
    private Timeline timeline;


    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        stage = primaryStage;

        this.scene = new Scene(FXMLLoader.load(getClass().getResource("/view/Principal.fxml")));

        stage.setScene(this.scene);
        stage.setTitle("Cronômetro.");
        stage.getIcons().add(new Image("/view/img/ampulheta64.png"));
        stage.setResizable(false);
        stage.show();
    }

    /**
     * Ação do botão Start.
     *
     * @author Daniel Silva Marcelino
     */
    @FXML
    private void actionButtonStart() {
        if (this.buttonStop.isDisable()) {
            buttonStop.setDisable(false);
        }
        if (!this.buttonStart.isDisable()) {
            buttonStart.setDisable(true);
        }

        timeline = new Timeline(new KeyFrame(Duration.seconds(0), event -> {

            // Contagem dos segundos.
            if (this.segundo < this.MINUTO) {
                if (this.segundo < this.DEZ) {
                    this.labelSegundos.setText("0" + this.segundo);
                } else {
                    this.labelSegundos.setText(String.valueOf(this.segundo));
                }
                this.segundo++;
            } else {
                this.labelSegundos.setText("00");
                this.segundo = 1;

                // FIM - Contagem dos minutos.
                if (this.minuto < this.MINUTO) {
                    if (this.minuto < this.DEZ) {
                        this.labelMinutos.setText("0" + this.minuto);
                    } else {
                        this.labelMinutos.setText(String.valueOf(this.minuto));
                    }
                    this.minuto++;
                } else {
                    this.labelMinutos.setText("00");
                    this.minuto = 1;

                    // FIM - Contagem das horas.
                    if (this.hora < this.HORA) {
                        if (this.hora < this.DEZ) {
                            this.labelHoras.setText("0" + this.hora);
                        } else {
                            this.labelHoras.setText(String.valueOf(this.hora));
                        }
                        this.hora++;
                    } else {
                        this.labelHoras.setText("00");
                        this.hora = 1;

                        // FIM - Contagem dos dias.
                        if (this.dia < this.DIAS) {
                            if (this.dia < this.DEZ) {
                                this.labelDias.setText("0" + this.dia);
                            } else {
                                this.labelDias.setText(String.valueOf(this.dia));
                            }
                            this.dia++;
                        } else {
                            this.labelDias.setText("000");
                            this.dia = 1;

                            // Contagem dos anos.
                            this.labelAnos.setText("ANO: " + this.ano);
                            this.ano++;
                            // FIM - Contagem dos anos.

                        }
                        // FIM - Contagem dos dias.
                    }
                    // FIM - Contagem das horas.

                }
                // FIM - Contagem dos minutos.

            }
            // FIM - Contagem dos segundos.

        }), new KeyFrame(Duration.seconds(1)));
        timeline.setCycleCount(Animation.INDEFINITE);
        timeline.play();
    }

    /**
     * Ação do botão Stop.
     *
     * @author Daniel Silva Marcelino
     */
    @FXML
    private void actionButtonStop() {
        if (this.buttonStart.isDisable()) {
            buttonStart.setDisable(false);
        }
        if (!this.buttonStop.isDisable()) {
            buttonStop.setDisable(true);
        }

        this.timeline.stop();// Parando o contador.

        // Resetando valores inicais.
        this.segundo = 1;
        this.minuto = 1;
        this.hora = 1;
        this.dia = 1;
        this.ano = 1;
        // FIM - Resetando valores inicais.

        // Zerando todos os labels.
        this.labelSegundos.setText("00");
        this.labelMinutos.setText("00");
        this.labelHoras.setText("00");
        this.labelAnos.setText("000");
        this.labelAnos.setText("ANO: 0");
        // FIM - Zerando todos os labels.

    }
}
