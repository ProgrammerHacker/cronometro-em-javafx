# README #
**# Cronômetro #**

Cronômetro feito em JavaFx no intuito de apenas organizar o tempo nas minhas mais diversas atividades em que se pode usá-lo.

### What is this repository for? ###

* O software é feito em JavaFx e pode ser utilizado em qualquer OS que rode uma JVM na versão 1.8.0_102 pra cima.

* 1.0 => versão com apenas a contagem simples do cronômetro contendo os segundos, minutos, horas, dias e anos.

### How do I get set up? ###

* A instalação do mesmo é muito simples, tem no repositório de cada versão o seu instalador para Windows e Linux. Não necessita de nenhuma configuração especial apenas abrir e usar.

### Contribution guidelines ###

* O software está sobre a licença GPL que pode ser encontrada uma cópia no diretório onde o software foi instalado bem como no diretório do código fonte aqui no Bitbucket no arquivo COPYING e ele deve ser seguido tudo o que diz a licença.

### Who do I talk to? ###

* Programador: Daniel Silva Marcelino <danielsystems.analyst@gmail.com> SOFTWARE ENGINEER.